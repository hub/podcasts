use anyhow::{anyhow, Result};
use image::imageops::FilterType;
use std::collections::HashMap;
use std::fmt::Display;
use std::path::Path;

use crate::download_covers::determin_cover_path;
use podcasts_data::ShowCoverModel;

// we only generate a fixed amount of thumbnails
// This enum is to avoid accidentally passing a thumb-size we didn't generate
#[derive(Copy, Clone, Hash, PartialEq, Eq)]
pub enum ThumbSize {
    Thumb64,
    Thumb128,
    Thumb256,
    Thumb512,
}
pub use self::ThumbSize::*;

impl ThumbSize {
    fn pixels(&self) -> f32 {
        match &self {
            Thumb64 => 64.0,
            Thumb128 => 128.0,
            Thumb256 => 256.0,
            Thumb512 => 512.0,
        }
    }
    pub fn hidpi(self, scale: i32) -> Option<ThumbSize> {
        // meh code
        if scale >= 5 {
            match self {
                Thumb64 => Some(Thumb512),
                Thumb128 => None,
                Thumb256 => None,
                Thumb512 => None,
            }
        } else if scale >= 3 {
            match self {
                Thumb64 => Some(Thumb256),
                Thumb128 => Some(Thumb512),
                Thumb256 => None,
                Thumb512 => None,
            }
        } else if scale >= 2 {
            match self {
                Thumb64 => Some(Thumb128),
                Thumb128 => Some(Thumb256),
                Thumb256 => Some(Thumb512),
                Thumb512 => None,
            }
        } else {
            Some(self)
        }
    }
}

impl Display for ThumbSize {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.pixels() as i32)
    }
}

pub async fn generate(
    pd: &ShowCoverModel,
    path: &Path,
) -> Result<HashMap<ThumbSize, gtk::gdk::Texture>> {
    let sizes: [ThumbSize; 4] = [Thumb64, Thumb128, Thumb256, Thumb512];
    // All thumbs must generate, we rely on them existing if the main image exists.
    let handles: Vec<_> = sizes
        .into_iter()
        .map(|size| {
            let pixels = size.pixels();
            let thumb_path = determin_cover_path(pd, Some(size));
            let path = path.to_path_buf();
            crate::RUNTIME.spawn(async move {
                let tmp_path = thumb_path.with_extension(".part");
                let tmp_path2 = tmp_path.clone();
                // save and read gdk texture
                let texture = crate::RUNTIME
                    .spawn_blocking(move || {
                        let image = image::io::Reader::open(path)?.decode()?;
                        image.resize(pixels as u32, pixels as u32, FilterType::Lanczos3);
                        image.save_with_format(&tmp_path2, image::ImageFormat::Png)?;
                        gtk::gdk::Texture::from_filename(&tmp_path2)
                            .map_err(|_| anyhow!("failed to read gtk texture"))
                    })
                    .await??;
                tokio::fs::rename(&tmp_path, &thumb_path).await?;
                Ok((size, texture))
            })
        })
        .collect();
    let result: Result<HashMap<ThumbSize, gtk::gdk::Texture>> =
        futures_util::future::join_all(handles)
            .await
            .into_iter()
            .map(|r| r.unwrap_or(Err(anyhow!("Failed to write cover thumbnail."))))
            .collect();
    result
}
